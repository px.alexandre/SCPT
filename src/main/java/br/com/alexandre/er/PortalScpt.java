package br.com.alexandre.er;

import java.io.IOException;

public class PortalScpt {

	public static void main(String[] args) {
		String TERMINAR = "0";
		String opcao;
		CadastroFio novoCadastro = new CadastroFio();
        do {
		   opcao = (Tela.montaCabecalho());
			if (opcao != TERMINAR) {
			    novoCadastro.cadastro(opcao);
			}    
		}
		while(!opcao.equals(TERMINAR));        
	}		

}
