package br.com.alexandre.er;

public class CadastroFio {
	
	private Fio fio[] = new Fio[10];
	
	
	public void  cadastro(String opcao){
		
		switch (opcao) {
		case "1":
			 inserirFio();
			 break;
		case "2":
			 alteraFio(this);
			 break;
		case "3":
			 excluirFio(this);
			 break;	
		case "4":
			listaFio(this);
		}
		
	
	}

	private static void listaFio(CadastroFio novoCadastro) {
				
		for(Fio fio: novoCadastro.fio) {
			if(fio!= null)
			  Tela.imprimeFio(fio);
		}
		
	}


	private void excluirFio(CadastroFio novoCadastro) {
		
		String codigo = Tela.obterCodigoDoFio();
		
		for(int i = 0; i<10; i++) {
			
			if ((novoCadastro.fio[i]!=null) && (codigo.equals(novoCadastro.fio[i].getCdfio()))) {
				Tela.imprimeFioExcluido(novoCadastro.fio[i]);
				novoCadastro.fio[i] = null;
			}	
		}
	}	
		
	private void alteraFio (CadastroFio novoCadastro) {
		
		String codigo = Tela.obterCodigoDoFio();
	
		for(int i = 0; i<10; i++) {
			if (novoCadastro.fio[i]!=null){
			  if(codigo.equals(novoCadastro.fio[i].getCdfio())) { 
			     Tela.imprimeFio(novoCadastro.fio[i]);
			     novoCadastro.fio[i] = Tela.alterarFio(novoCadastro.fio[i]);
			     break;
			  } 
			}
		}
	}


	private void inserirFio() {
		
		for (int i =0; i<10; i++ ) {
			if (this.fio[i]==null){
		        this.fio[i] = Tela.montaTelaInserirFio();
		        break;
		    }
		}	
    }
}
