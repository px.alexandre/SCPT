package br.com.alexandre.er;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.com.alexandre.util.Teclado;

public class Tela {
    
	public static String montaCabecalho() {
		 Calendar cal = Calendar.getInstance(new Locale("pt", "BR"));
		 Date data = cal.getTime();
		 String mascara = "dd/MMM/YY HH':'mm':'ss";
		 SimpleDateFormat formatador = new SimpleDateFormat(mascara);
		 		 
		 show("========= Sistema de controle da produção - TECELAGEM ============= \n");
		 show("= ----- PORTAL DO LAR ------------ "+ formatador.format(data)+ " ----------- = \n");
		 show("= -- Cadastro de fios                                             = \n");
		 show("= 1 - Inserir                                                     = \n");
		 show("= 2 - Alterar                                                     = \n");
	     show("= 3 - Excluir                                                     = \n");
	     show("= 4 - Listar                                                      = \n");
	     show("= 0 - Sair                                                        = \n");
	     show("= Opcao...: ");
	     String opcao = Teclado.le();

	     return opcao;
	}	
		
    public static Fio montaTelaInserirFio() {
    	
    	Fio novofio = new Fio();
    	show("========= Inserir fio ============================================== \n");
    	show("== Código ..: ");
    	novofio.setCdfio(Teclado.le());
    	show("== Tipo ....: ");
    	novofio.setTipo(Teclado.le());
    	show("== Descicao.: ");
    	novofio.setDescricao(Teclado.le());
		return novofio;
    	
    }

	static void show(String text){
		System.out.print(text);
	}

	static void showFormat(String text, String string){
		System.out.printf(text, string);
	}
	
	public static void imprimeFio(Fio fio) {
		show("== -------------------------------------- == \n");
		showFormat("== Codigo.....: %s \n", fio.getCdfio());
		showFormat("== Tipo.......: %s \n", fio.getTipo());
		showFormat("== Descricao..: %s \n", fio.getDescricao());
	}

	
	public static String obterCodigoDoFio() {
		// TODO Auto-generated method stub
		show("== -- Portal da Cor  --------------------------- = \n");
		show("== Digite o codigo do Fio...: ");
		String codFio = Teclado.le();
		return codFio;
		
		
	}

	public static Fio alterarFio(Fio fio) {
		show("== ---- Digite os novos Campos para alteração ---- == \n");
		show("== Tipo ....: ");
    	fio.setTipo(Teclado.le());
    	show("== Descicao.: ");
    	fio.setDescricao(Teclado.le());
	    return fio;
	}

	public static void imprimeFioExcluido(Fio fio) {
		show("== ------- Fio excluido ---------------------- == \n");
		showFormat("== Codigo.....: %s \n", fio.getCdfio());
		showFormat("== Tipo.......: %s \n", fio.getTipo());
		showFormat("== Descricao..: %s \n", fio.getDescricao());
		
	}
}
