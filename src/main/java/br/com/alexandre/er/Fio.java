package br.com.alexandre.er;

public class Fio {

	private String cdfio;
	private String tipo;
	private String descricao;
	
	public String getCdfio() {
		return cdfio;
	}
	public void setCdfio(String cdfio) {
		this.cdfio = cdfio;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
